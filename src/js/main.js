// const divApp = document.getElementById('app');
// divApp.innerText = 'Hello Javascript'

const inputNumber = document.getElementById('inputNumber')
const equalButton = document.getElementById('equalButton')
const notEqualButton = document.getElementById('notEqualButton')
const greaterButton = document.getElementById('greaterButton')
const greaterThanOrEqualButton = document.getElementById('greaterThanOrEqualButton')

const output = document.getElementById('output')

equalButton.addEventListener('click', function () {
    const value =  inputNumber.value
    const result = value === 10
    output.innerText = result
    console.log(output.innerText)
})

notEqualButton.addEventListener('click', function () {
    const value =  inputNumber.value
    const result = value !== 10
    output.innerText = result
    console.log(output)
})

greaterButton.addEventListener('click', function () {
    const value =  inputNumber.value
    const result = value > 10
    output.innerText = result
    console.log(output)
})

greaterThanOrEqualButton.addEventListener('click', function () {
    const value =  inputNumber.value
    const result = value >= 10
    output.innerText = result
    console.log(output)
})


const sample = {
    a:'A',
    b:'B',
}



const rr = sample.a
sample.a = 'wwww'

function display(obj){
    return obj.a +' ,' + obj.b
}

const result = display(sample);
const result2 = display({
    a:'A',
    b:'B',
});

console.log(result);
console.log(result2) ;