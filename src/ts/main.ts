// ----Exercise 1 -----
interface Menu {
    name: string
    subMenu: SubMenu[]
}

interface SubMenu {
    name: string
}

const menus: Menu[] = [
    {
        name: 'Home',
        subMenu: [],
    },
    {
        name: 'About',
        subMenu: [
            {
                name: 'Company',
            },
            {
                name: 'Team',
            },
        ],
    },
    {
        name: 'Products',
        subMenu: [
            {
                name: 'Electronics',
            },
            {
                name: 'Clothing',
            },
            {
                name: 'Accessories',
            },
        ],
    },
    {
        name: 'Services',
        subMenu: [],
    },
    {
        name: 'Contact',
        subMenu: [
            {
                name: 'Phone',
            },
        ],
    },
    {
        name: 'Blog',
        subMenu: [],
    },
    {
        name: 'Gallery',
        subMenu: [
            {
                name: 'Photos',
            },
            {
                name: 'Videos',
            },
            {
                name: 'Events',
            },
        ],
    },
    {
        name: 'FAQ',
        subMenu: [],
    },
    {
        name: 'Downloads',
        subMenu: [
            {
                name: 'Documents',
            },
            {
                name: 'Software',
            },
        ],
    },
    {
        name: 'Support',
        subMenu: [
            {
                name: 'Help Center',
            },
            {
                name: 'Contact Us',
            },
            {
                name: 'Knowledge Base',
            },
        ],
    },
];

const divEx1 = document.getElementById('exercise1') as HTMLDivElement;
const ul = document.createElement('ul')

divEx1.appendChild(ul);
let i: number = 0;
let j: number = 0;
for (i = 0; i < menus.length; ++i) {
    let li = document.createElement('li');
    li.innerText = menus[i].name + "";
    ul!.appendChild(li);

    if (menus[i].subMenu.length != 0) {
        let subUl = document.createElement('ul');
        li.appendChild(subUl);
        for (j = 0; j < menus[i].subMenu.length; ++j) {
            let subLi = document.createElement('li');
            subLi.innerText = menus[i].subMenu[j].name + '';
            subUl.appendChild(subLi);
        }
    }
}




// ---Excercise 2---
interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
}

const questions: Question[] = [
    {
        question: 'What is the output of the following code?\n\nconsole.log(typeof null);',
        choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to add one or more elements to the end of an array?',
        choices: ['push()', 'join()', 'slice()', 'concat()'],
        correctAnswer: 0,
    },
    {
        question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
        choices: ['"327"', '"12"', '"57"', '"NaN"'],
        correctAnswer: 2,
    },
    {
        question: 'What is the purpose of the "use strict" directive in JavaScript?',
        choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
        correctAnswer: 2,
    },
    {
        question: 'What is the scope of a variable declared with the "let" keyword?',
        choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
        correctAnswer: 2,
    },
    {
        question: 'Which higher-order function is used to transform elements of an array into a single value?',
        choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
        correctAnswer: 2,
    },
    {
        question: 'What does the "=== " operator in JavaScript check for?',
        choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
        correctAnswer: 1,
    },
    {
        question: 'What is the purpose of the "this" keyword in JavaScript?',
        choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
        correctAnswer: 3,
    },
    {
        question: 'What does the "NaN" value represent in JavaScript?',
        choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
        correctAnswer: 0,
    },
    {
        question: 'Which method is used to remove the last element from an array?',
        choices: ['pop()', 'shift()', 'slice()', 'splice()'],
        correctAnswer: 0,
    },
];



const div = document.getElementById('exercise2') as HTMLDivElement

const score = document.createElement('div')

let total = 0;


score.innerText = 'Current Score: ' + total + '/10';
const br = document.createElement('br');
div.appendChild(score)
div.appendChild(br)

for (const item in questions) {
    const form = document.createElement('form');
    const divQuest = document.createElement('div')
    const quest = document.createElement('p')
    const br = document.createElement('br');
    const p = document.createElement('p')
    const answer = questions[item].correctAnswer;
    quest.innerText = questions[item].question
    div.appendChild(form)
    form.appendChild(divQuest)
    divQuest.appendChild(quest)
    divQuest.appendChild(br)
    // เมื่อฟอร์มถูกส่ง



    for (const x in questions[item].choices) {
        const choice = document.createElement('input')
        const div2 = document.createElement('div')
        const btn = document.createElement('input')
        btn.type = 'submit'
        btn.value = 'Submit'
        choice.type = 'radio'
        choice.name = item
        choice.value = questions[item].choices[x]
        divQuest.appendChild(div2)

        div2.appendChild(choice)

        const space = document.createElement('label')
        space.innerText = questions[item].choices[x]
        div2.appendChild(space)

        if (+x == questions[item].choices.length - 1) {
            divQuest.appendChild(btn)
        }

        divQuest.appendChild(p)


    }

    form.addEventListener('submit', (event) => {
        event.preventDefault();

        try {
            const chooseAnswer = document.querySelector(`input[name="${item}"]:checked`)!;
            const choose = chooseAnswer.getAttribute('value')!

            if (questions[item].choices[answer] === choose) {


                p.innerText = 'Correct!'
                divQuest.appendChild(p)
                total++;
                score.innerText = 'Current Score: ' + total + '/10';
            } else {

                p.innerText = 'Incorrect!'
                divQuest.appendChild(p)
                score.innerText = 'Current Score: ' + total + '/10';
            }
            const formElement = event.target as HTMLFormElement;
            const submitButton = formElement.querySelector('input[type="submit"]') as HTMLInputElement;
            const options = formElement.querySelectorAll('input[type="radio"]');
            options.forEach((option) => {
                (option as HTMLInputElement).disabled = true;
            });

            const nameButton = formElement.querySelectorAll('label');
            nameButton.forEach((option) => {
                (option as HTMLLabelElement).style.pointerEvents = 'none';
                (option as HTMLLabelElement).style.opacity = '0.5';
            });
            if (submitButton) {
                submitButton.disabled = true;
            }
        } catch {
            alert('please choose an answer first!');
        }

    });
}


export {

}